var _ = require('lodash');

module.exports = function(app) {
  var Role = app.models.Role;
  // var User = app.models.user;
  // var Collaboration = app.models.Collaboration;
  var Map = app.models.Map;
  var Project = app.models.Project;

  Role.registerResolver('mapuser', function(role, context, cb) {

    function reject() {
      process.nextTick(function() {
        cb(null, false);
      });
    }
    // if the target model is not project
    if (context.modelName !== 'Map') {
      return reject();
    }

    // do not allow anonymous users
    var userId = context.accessToken.userId;
    if (!userId) {
      return reject();
    }

    var mapId = context.modelId;
    console.log("mapId ", mapId);


    Map.findById(mapId, { fields: ['projectId']}, function(err, map) {
      if (err || !map)
        return reject();

      console.log("map ");
      console.log(map);

      var projectId = map.projectId;
      console.log("map -> project");
      console.log(projectId);

      Project.findById(projectId, { include: { relation: 'users', scope: { fields: ['id', 'username'] }}} , function (err, project) {
        if (err) throw err;

        var users = project.users();

        console.log("map -> users");
        console.log(users);

        var validUsersIds = _.map(users, function (user) {
          return user.id;
        });

        var indexPosition = _.indexOf(validUsersIds, parseInt(userId));
        console.log("map -> indexPosition");
        console.log(indexPosition);

        if (indexPosition > -1) {

          return cb(null, true);
        }

        return cb(null, false);
      });
    });
  });
};
