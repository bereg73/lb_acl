var _ = require('lodash');

module.exports = function(app) {
  var Role = app.models.Role;
  var User = app.models.user;

  Role.registerResolver('projectuser', function(role, context, cb) {

    function reject() {
      process.nextTick(function() {
        cb(null, false);
      });
    }

    // if the target model is not project
    if (context.modelName !== 'project') {
      return reject();
    }

    // do not allow anonymous users
    var userId = context.accessToken.userId;
    if (!userId) {
      return reject();
    }

    // check if userId is in team table for the given project id
    context.model.findById(context.modelId, function(err, project) {
      if (err || !project)
        return reject();


      User.findById(userId, { include: 'projects' }, function (err, user) {
          if (err) throw err;

        var userProjects = user.projects();
        var projectsIds = _.map(userProjects, function (project) {
          return project.id;
        });

        var indexPosition = _.indexOf(projectsIds, parseInt(context.modelId));

        if (indexPosition > -1) {

          return cb(null, true);
        }

        return cb(null, false);
      });



      /*
            User.findById(userId, function (err, user) {
              if (err) throw err;

              console.log(user.projects);
            });
      */
/*


      var projects_1_ids = [1, 2];
      var projects_2_ids = [3];
      console.log("userId");
      console.log(userId);

      console.log("context.modelId");
      console.log(context.modelId);

      console.log("project");
      console.log(project);


      console.log("projects_1_ids.indexOf(context.modelId)");
      console.log(projects_1_ids.indexOf(parseInt(context.modelId)));

      if (userId == 2 && (projects_1_ids.indexOf(parseInt(context.modelId)) > -1)) {

          console.log("User ", userId, " can view project # ", context.modelId);
          return cb(null, true);

      }
      else if (userId == 3 && (projects_2_ids.indexOf(parseInt(context.modelId)) > -1)) {

          console.log("User ", userId, " can view project # ", context.modelId);
          return cb(null, true);

      }
      // else {

        console.log("THIS User ", userId, " CANNOT view project # ", context.modelId);
        return cb(null, false);
      // }
*/


      // return cb(null, true);
    });
  });
};
